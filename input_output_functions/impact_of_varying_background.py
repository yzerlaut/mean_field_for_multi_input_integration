import sys
sys.path.append('../')
from mean_field.euler_method import run_mean_field
from network_simulations.waveform_input import double_gaussian, smooth_heaviside

sys.path.append('../code')
from my_graph import set_plot

import numpy as np
import matplotlib.pylab as plt
import matplotlib as mpl

def heaviside(x):
    return .5*(1+np.sign(x))
def step_input(t, T0, amp, T1=0.02):
    return amp*heaviside(t-T0)*(1-np.exp(-(t-T0)/T1))
    # return 0*amp*np.exp(-(t-T0)**2/2./T1**2)


t0, T1, T2, tstop = 200e-3, 50e-3, 70e-3, 400e-3
amp_max = 15.
amp0 = 1

if sys.argv[-1]=='full':

    fig1, [ax1, ax2] = plt.subplots(2, figsize=(5,6))
    plt.subplots_adjust(left=.25, bottom=.25 )

    N=25
    
    max_f_amp, max_vm_amp = np.zeros(N), np.zeros(N)
    max_fe_amp, max_fi_amp = np.zeros(N), np.zeros(N)
    amplitudes = np.linspace(0, amp_max, N)
    
    for i in range(N):
        amp = amplitudes[i]
        def func(t):
            return double_gaussian(t, t0, T1, T2, amp0)

        t, fe, fi, muV, sV, muG, Tv = run_mean_field('RS-cell', 'FS-cell', 'CONFIG1', func, T=5e-3,\
                                                     ext_drive_change=amp,
                                                     afferent_exc_fraction=None,
                                                     extended_output=True, tstop=tstop)
        max_f_amp[i] = np.max(.8*fe+.2*fi)-.8*fe[0]-.2*fi[0]
        max_fe_amp[i] = np.max(fe)-fe[0]
        max_fi_amp[i] = np.max(fi)-fi[0]
        max_vm_amp[i] = np.max(1e2*np.abs((muV-muV[0])/muV[0]))

    ax1.plot(amplitudes, max_f_amp, 'k-', lw=3)
    # ax1.fill_between(amplitudes, max_f_amp, max_f_amp[0]+0*max_f_amp, color='lightgray')
    ax1.plot([0], [0], 'wD', ms=0.1)
    set_plot(ax1, ['left'], ylabel='max. $\delta \\nu$ (Hz)', xticks=[])
    ax2.plot(amplitudes, max_vm_amp, 'k-', lw=3)
    ax2.plot([0], [0], 'wD', ms=0.1)
    ax2.fill_between(amplitudes, max_vm_amp, max_vm_amp[0]+0*max_vm_amp, color='lightgray')
    set_plot(ax2, ylabel=r'max. $\| \delta V/V_0 \| $ %', xlabel='$\\nu_e^{drive}$ (Hz)')
    plt.show()

else:

    fig1, [ax1, ax2, ax3] = plt.subplots(3, figsize=(5,6))
    plt.subplots_adjust(left=.25, bottom=.25 )

    for amp in np.linspace(0, amp_max, 10):
        def func(t):
            # return step_input(t, 0.02, 1.)*amp
            return double_gaussian(t, t0, T1, T2, 5.)

        t, fe, fi, muV, sV, muG, Tv = run_mean_field('RS-cell', 'FS-cell', 'CONFIG1', func, T=5e-3,\
                                                     ext_drive_change=amp,
                                                     afferent_exc_fraction=None,
                                                     extended_output=True, tstop=tstop)
        ax1.plot(1e3*t, amp+func(t), 'k')
        ax2.plot(1e3*t, .8*fe+.2*fi, 'k')
        ax3.plot(1e3*t, 1e2*np.abs((muV-muV[0])/muV[0]), 'k')

    set_plot(ax1, ['left'], ylabel='$\\nu_e^{aff}$ (Hz)', xticks=[])
    set_plot(ax2, ['left'], ylabel='$\\nu$ (Hz)', xticks=[])
    set_plot(ax3, ylabel=r'$\| \delta V/V_0 \| $ %', xlabel='time (ms)')
    plt.show()


